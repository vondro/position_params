#!/usr/bin/env python
 
PACKAGE='position_params'
import rospy
import roslib;roslib.load_manifest(PACKAGE)
from std_msgs.msg import String
import dynamic_reconfigure.client

rospy.init_node("position_params")

def callback(config):
    # rospy.loginfo("Config set to {int_param}, {double_param}, {str_param}, {bool_param}, {size}".format(**config))
    pass

def set_param(req):
    client = dynamic_reconfigure.client.Client("/fcu/fcu", timeout=2, config_callback=callback)
    client.update_configuration({"position_control":req.data})

def listener():
    subscriber = rospy.Subscriber('/fcu/position_params', String, set_param)
    rospy.spin()

if __name__ == "__main__":
    listener()


